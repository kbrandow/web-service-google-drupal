<?php

/**
 * @file
 * The Web Services Drupal-Google integration class
 */

/**
 * The Web Services Drupal-Google integration class
 */
class WSDrupalGoogle {

  protected $file_name;


  public function get_file_name() {
    return "The filename is: " . $this->file_name;
  }

  public function set_file_name($file_name) {
    $this->file_name = $file_name;
    return TRUE;
  }

  public function __construct($file_name) {
    $this->file_name = $file_name;
  }

}
