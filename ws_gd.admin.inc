<?php
/**
 * @file
 * This file contains functions & forms to administer the Go Ask Alice! Jira
 * integration module
 */

/**
 * Form builder; Configure settings for Jira integration
 */
function ws_gd_admin_settings_form() {
  $form = array();
  $form['ws_gd_app_name'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_app_name', ''),
    '#title' => t('Google API Application Name'),
    '#required' => TRUE,
    '#description' => t('The name of this application in the Google API Console'),
  );
  $form['ws_gd_client_id'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_client_id', ''),
    '#title' => t('Google API Client ID'),
    '#required' => TRUE,
    '#description' => t('The Client ID obtained from Google API Console'),
  );
  $form['ws_gd_client_secret'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_client_secret', ''),
    '#title' => t('Google API Client Secret'),
    '#required' => TRUE,
    '#description' => t('The Client secret obtained from Google API Console'),
  );
  $form['ws_gd_redirect_uri'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_redirect_uri', ''),
    '#title' => t('Google API Redirect URI'),
    '#required' => TRUE,
    '#description' => t('The Redirect URI secret obtained from Google API Console'),
  );
  $form['ws_gd_access_token'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_access_token', ''),
    '#title' => t('Google API Access Token'),
    '#required' => TRUE,
    '#description' => t('The Access Token for the Google User who\'s Google Drive account will be accessed.'),
  );
  $form['ws_gd_refresh_token'] = array(
    '#type' => 'textfield',
    '#default_value' => variable_get('ws_gd_refresh_token', ''),
    '#title' => t('Google API Refresh Token'),
    '#required' => TRUE,
    '#description' => t('The Refresh Token for the Google User who\'s Google Drive account will be accessed.'),
  );

  return system_settings_form($form);
}

/**
 * Form validation function for gaa_jira_admin_settings_form
 */
function ws_gd_admin_settings_form_validate($form, &$form_state) {
  $values = $form_state['values'];

}

