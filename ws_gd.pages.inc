<?php

function ws_gd_doc_dev() {
  // function ws_gd_boot_drive_client
  $refresh_token = variable_get('ws_gd_refresh_token');
  $access_token = variable_get('ws_gd_access_token');
  define('REFRESH_TOKEN', '1/upARWacpxJR_Fyrt02zTqce4BdRLL_T8S6hYWmPPSEo');

  $client = ws_gd_boot_client();
  $drive = new Google_DriveService($client);

  // function ws_gd_access_token(&$client, $access_token)
  $client->setAccessToken($access_token);
  if($client->isAccessTokenExpired()) {
    $client->refreshToken($refresh_token);
    variable_set('ws_gd_access_token', $client->getAccessToken());
  }

  $files = $drive->files;
  $permissions = $drive->permissions;
  $file_permissions = $permissions->listPermissions('1TbwCP4ZuMa5Jh7GYsdIZ9MXDjYUeFytHBiWbjme9YLs');
  $list = $files->listFiles();
  dpm($list);
  $file = $files->get('1TbwCP4ZuMa5Jh7GYsdIZ9MXDjYUeFytHBiWbjme9YLs');
  dpm($file);
  $fileVars = get_object_vars((object)$file);
  $html = $file['exportLinks']['text/html'];
  $request = new Google_HttpRequest($html, 'GET', null, null);
  $httpRequest = Google_Client::$io->authenticatedRequest($request);
  $content = $httpRequest->getResponseBody();
  $fileVars['content'] = $content?($content):'';

  // Create function from this to set user permission for a file, should be used
  // when a question is passed from one user to another
  // i.e. When a question is passed from a researcher to a writer the write
  // permission should be enabled for the writer and the writer permission
  // should be replaced with a reader permission for the researcher
  $value = 'kris.brandow@gmail.com';
  $type = 'user';
  $role = 'reader';
  $fileId = '1TbwCP4ZuMa5Jh7GYsdIZ9MXDjYUeFytHBiWbjme9YLs';
  $permissionId = '14656267068238039642';
  $newRole = 'writer';
  updatePermission($drive, $fileId, $permissionId, $newRole);
  // Extracts the body from the HTML
  // Clean up!
  //
  // We need a function that will cleanly extract the body from the HTML when
  // passed in the entire contents of an HTML page.
  $string = $fileVars['content'];
  $d = new DOMDocument;
  $mock = new DOMDocument;
  $d->loadHTML($string);
  $body = $d->getElementsByTagName('body')->item(0);
  foreach ($body->childNodes as $child){
    $mock->appendChild($mock->importNode($child, true));
  }

  $html = $mock->saveHTML();


  return $html;
}

/**
 * Insert a new permission.
 *
 * @param Google_DriveService $service Drive API service instance.
 * @param String $fileId ID of the file to insert permission for.
 * @param String $value User or group e-mail address, domain name or NULL for
                       "default" type.
 * @param String $type The value "user", "group", "domain" or "default".
 * @param String $role The value "owner", "writer" or "reader".
 * @return Google_Permission The inserted permission. NULL is returned if an API
                             error occurred.
 */
function insertPermission($service, $fileId, $value, $type, $role) {
  $newPermission = new Google_Permission();
  $newPermission->setValue($value);
  $newPermission->setType($type);
  $newPermission->setRole($role);
  try {
    return $service->permissions->insert($fileId, $newPermission);
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
  return NULL;
}

/**
 * Update a permission's role.
 *
 * @param Google_DriveService $service Drive API service instance.
 * @param String $fileId ID of the file to update permission for.
 * @param String $permissionId ID of the permission to update.
 * @param String $newRole The value "owner", "writer" or "reader".
 * @return Google_Permission The updated permission. NULL is returned if an API error occurred.
 */
function updatePermission($service, $fileId, $permissionId, $newRole) {
  try {
    // First retrieve the permission from the API.
    $data = $service->permissions->get($fileId, $permissionId);
    $permission = new Google_Permission($data);
    $permission->setRole($newRole);
    return $service->permissions->update($fileId, $permissionId, $permission);
  } catch (Exception $e) {
    print "An error occurred: " . $e->getMessage();
  }
  return NULL;
}

function ws_gd_boot_client() {
  $base = realpath('.') . '/' . libraries_get_path('google-api-php-client') . '/src';
  require_once "{$base}/Google_Client.php";
  require_once "{$base}/contrib/Google_DriveService.php";

  $client = new Google_Client();
  $client->setApplicationName(variable_get('ws_gd_app_name', ''));
  $client->setClientId(variable_get('ws_gd_client_id', ''));
  $client->setClientSecret(variable_get('ws_gd_client_secret', ''));
  $client->setRedirectUri(variable_get('ws_gd_redirect_uri', ''));

  return $client;
}
